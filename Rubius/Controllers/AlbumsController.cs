﻿using Microsoft.AspNetCore.Mvc;
using Rubius.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Controllers
{
    [Produces("application/json")]
    [Route("api/Albums")]
    public class AlbumsController:BaseController
    {
        /// <summary>
        /// Фильтр альбомов
        /// </summary>
        /// <param name="attributes">Атрибуты фильтрации</param>
        /// <param name="id">Id исполнителя</param>
        [HttpGet("[action]/{id}")]
        public IEnumerable<Album> Filter([FromQuery] AlbumFilterAttributes attributes, int id)
        {
            return _musicians.SingleOrDefault(m => m.Id == id).Albums
                .Where(x =>
                (attributes.Name != null ? x.Name.ToLower().Contains(attributes.Name.ToLower()) : true) &&
                (attributes.MaxReleaseYear != null ? int.Parse(x.ReleaseYear) <= attributes.MaxReleaseYear : true) &&
                (attributes.MinReleaseYear != null ? int.Parse(x.ReleaseYear) >= attributes.MinReleaseYear : true));
        }


        [HttpGet("[action]/{id}")]
        public Album ResetPlays(int id)
        {
            Album album = _albums.SingleOrDefault(x => x.Id == id);
            List<Track> tracks = new List<Track>();
            foreach (Track track in album.Tracks)
            {
                track.IsListened = false;
                tracks.Add(track);
            }
            album.Tracks = tracks;
            SaveAlbum(album);
            return _albums.SingleOrDefault(x => x.Id == id);
        }
    }
}
