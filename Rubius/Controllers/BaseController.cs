﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Rubius.Models;
using System.Collections.Generic;
using System.Linq;

namespace Rubius.Controllers
{
    public class BaseController:Controller
    {
        private DataService service;
        public BaseController()
        {
            service = new DataService();
        }
        private Data jsonData
        {
            get => service.JsonData.ToObject<Data>();
            set => service.Data = JsonConvert.SerializeObject(value);
        }

        protected List<Musician> _musicians => jsonData.Musicians.ToList();
        protected List<Album> _albums => _musicians.SelectMany(m => m.Albums).ToList();
        protected List<Track> _tracks => _albums.SelectMany(a => a.Tracks).ToList();

        protected Album ParentOf(Track track)
        {
            return _albums.SingleOrDefault(a => a.Tracks.Contains(track));
        }

        protected Musician ParentOf(Album album)
        {
            return _musicians.SingleOrDefault(m => m.Albums.Contains(album));
        }

        protected void SaveTrack(Track track)
        {
            Album album = ParentOf(track);
            album.Tracks[album.Tracks.IndexOf(track)] = track;
            SaveAlbum(album);

        }

        protected void SaveAlbum(Album album)
        {
            Musician musician = ParentOf(album);
            musician.Albums[musician.Albums.IndexOf(album)] = album;
            SaveMusician(musician);
        }

        protected void SaveMusician(Musician musician)
        {
            var musicians = _musicians;
            musicians[musicians.IndexOf(musician)] = musician;
            jsonData = new Data(musicians);
        }
    }
}