﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rubius.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Rubius.Controllers
{
    [Produces("application/json")]
    [Route("api/Musicians")]
    public class MusiciansController : BaseController
    {
        private DataService service = new DataService();
        
        [HttpGet("[action]")]
        public IEnumerable<Musician> GetAll()
        {
            return _musicians;
        }

        [HttpGet("[action]")]
        public IEnumerable<Musician> Filter([FromQuery] MusicianFilterAttributes attributes)
        {
            return _musicians
                .Where(x =>
                (attributes.Name != null ? x.Name.ToLower().Contains(attributes.Name.ToLower()) : true) &&
                (attributes.Genre != null ? x.Genre.ToLower().Contains(attributes.Genre.ToLower()) : true) &&
                (attributes.MaxAge != null ? int.Parse(x.Age) <= attributes.MaxAge : true) &&
                (attributes.MinAge != null ? int.Parse(x.Age) >= attributes.MinAge : true));
        }

        //Это метод фильтрации по дереву, однако я не нашел ему применения в моем интерфейсе
        [HttpGet("[action]")]
        public IEnumerable<Musician> FilterTree([FromQuery] DataFilterAttributes attributes)
        {
            var musicians = _musicians;
            foreach (var musician in _musicians)
            {
                if (!musician.Genre.ToLower().Contains(attributes.Genre.ToLower()))
                {
                    musicians.Remove(musician);
                }
                else
                {
                    foreach (var album in musician.Albums)
                    {
                        if ((attributes.MaxAlbumYear != null ? int.Parse(album.ReleaseYear) > attributes.MaxAlbumYear : true) ||
                            (attributes.MinAlbumYear != null ? int.Parse(album.ReleaseYear) < attributes.MinAlbumYear : true))
                        {
                            musicians.SingleOrDefault(x => x.Id == musician.Id).Albums.Remove(album);
                        }
                        else
                        {
                            foreach (var track in album.Tracks)
                            {
                                if (attributes.FavoritesOnly ? !track.IsFavorite : false)
                                {
                                    musicians.SingleOrDefault(m => m.Id == musician.Id).Albums.SingleOrDefault(a => a.Id == album.Id).Tracks.Remove(track);
                                }
                            }
                        }
                    }
                }
            }
            return musicians;
        }
    }
}
