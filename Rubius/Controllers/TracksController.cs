﻿using Microsoft.AspNetCore.Mvc;
using Rubius.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Controllers
{
    [Produces("application/json")]
    [Route("api/Tracks")]
    public class TracksController :BaseController
    {
        [HttpGet("[action]/{id}")]
        public Track Like(int id)
        {
            Track track = _tracks.SingleOrDefault(x => x.Id == id);
            if (!track.IsLiked)
            {
                track.Rating++;
            }
            else
            {
                track.Rating--;
            }
            track.IsLiked = !track.IsLiked;
            SaveTrack(track);
            return _tracks.SingleOrDefault(x => x.Id == track.Id);
        }

        [HttpGet("[action]/{id}")]
        public Track ListenTrack(int id)
        {
            Track track = _tracks.SingleOrDefault(x => x.Id == id);
            track.IsListened = true;
            SaveTrack(track);
            return _tracks.SingleOrDefault(x => x.Id == track.Id);
        }

        [HttpGet("[action]/{id}")]
        public Track AddToFavorites(int id)
        {
            Track track = _tracks.SingleOrDefault(x => x.Id == id);
            track.IsFavorite = !track.IsFavorite;
            SaveTrack(track);
            return _tracks.SingleOrDefault(x => x.Id == track.Id);
        }

        /// <summary>
        /// Фильтр треков
        /// </summary>
        /// <param name="attributes">Атрибуты фильтрации</param>
        /// <param name="id">Id альбома</param>
        [HttpGet("[action]/{id}")]
        public IEnumerable<Track> Filter([FromQuery] TrackFilterAttributes attributes, int id)
        {
            return _albums.SingleOrDefault(a => a.Id == id).Tracks
                .Where(x =>
                (attributes.Name != null ? x.Name.ToLower().Contains(attributes.Name.ToLower()) : true) &&
                (attributes.FavoritesOnly ? x.IsFavorite : true));
        }
    }
}
