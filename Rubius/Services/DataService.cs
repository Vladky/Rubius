﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Controllers
{
    public class DataService
    {
        private string dataPath = @".\Data\data.json";
        public string Data
        {
            get => System.IO.File.ReadAllText(dataPath);
            set
            {
                System.IO.File.WriteAllText(dataPath, value);
            }
        }
        public JObject JsonData
        {
            get => (JObject)JsonConvert.DeserializeObject(System.IO.File.ReadAllText(dataPath));
            set
            {
                var data = JsonConvert.SerializeObject(value);
                System.IO.File.WriteAllText(dataPath, data);
            }
        }

        public DataService() { }
    }
}
