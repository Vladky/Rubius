﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Models
{
    public class Track : IEquatable<Track>
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("duration")]
        public string Duration { get; set; }
        [JsonProperty("isFavorite")]
        public bool IsFavorite { get; set; }
        [JsonProperty("isListened")]
        public bool IsListened { get; set; }
        [JsonProperty("isLiked")]
        public bool IsLiked { get; set; }
        [JsonProperty("rating")]
        public int Rating { get; set; }

        public bool Equals(Track other)
        {
            return Id == other.Id;
        }
    }

    public class TrackFilterAttributes
    {
        public string Name { get; set; }
        public bool FavoritesOnly { get; set; }
    }
}
