﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Models
{
    public class DataFilterAttributes
    {
        public bool FavoritesOnly { get; set; } = false;
        public string Genre { get; set; }
        public int? MinAlbumYear { get; set; }
        public int? MaxAlbumYear { get; set; }
        public DataFilterAttributes()
        {
            Genre = "";
        }
    }
}
