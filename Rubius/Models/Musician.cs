﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Models
{

    public class Musician : IEquatable<Musician>
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("age")]
        public string Age { get; set; }
        [JsonProperty("genre")]
        public string Genre { get; set; }
        [JsonProperty("careerStartYear")]
        public string CareerStartYear { get; set; }
        [JsonProperty("albums")]
        public List<Album> Albums { get; set; }

        public bool Equals(Musician other)
        {
            return Id == other.Id;
        }

        public Musician()
        {
            Albums = new List<Album>();
        }
    }

    public class MusicianFilterAttributes
    {
        public string Name { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public string Genre { get; set; }
        public MusicianFilterAttributes()
        {
            Name = "";
            Genre = "";
        }
    }
}
