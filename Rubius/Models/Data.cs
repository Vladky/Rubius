﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Models
{

    public class Data
    {
        [JsonProperty("musicians")]
        public List<Musician> Musicians { get; set; }
        public Data(List<Musician> musicians)
        {
            Musicians = musicians;
        }
    }
}
