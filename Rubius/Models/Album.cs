﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rubius.Models
{
    public class Album : IEquatable<Album>
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("releaseYear")]
        public string ReleaseYear { get; set; }
        [JsonProperty("tracks")]
        public List<Track> Tracks { get; set; }

        public bool Equals(Album other)
        {
            return Id == other.Id;
        }

        public Album()
        {
            Tracks = new List<Track>();
        }
    }

    public class AlbumFilterAttributes
    {
        public string Name { get; set; }
        public int? MinReleaseYear { get; set; }
        public int? MaxReleaseYear { get; set; }
        public AlbumFilterAttributes()
        {
            Name = "";
        }
    }
}
