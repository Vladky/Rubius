﻿import { Injectable, Inject } from '@angular/core';
import { Http, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { MusicianComponent } from "./musician/musician.component";

@Injectable()
export class DataService {
    private musiciansUrl: string = 'api/Musicians';
    private albumsUrl: string = 'api/Albums';
    private tracksUrl: string = 'api/Tracks';
    constructor(
        private http: Http,
        @Inject('BASE_URL') private baseUrl: string, ) {

    }

    public getData() {
        return this.http.get(`${this.baseUrl}/${this.musiciansUrl}/GetAll`);
    }
    public like(id: number) {
        return this.http.get(`${this.baseUrl}/${this.tracksUrl}/Like/${id}`);
    }
    public listen(id: number) {
        return this.http.get(`${this.baseUrl}/${this.tracksUrl}/ListenTrack/${id}`);
    }
    public addToFavorites(id: number) {
        return this.http.get(`${this.baseUrl}/${this.tracksUrl}/AddToFavorites/${id}`);
    }
    public resetPlays(id: number) {
        return this.http.get(`${this.baseUrl}/${this.albumsUrl}/ResetPlays/${id}`);
    }
    public filterMusicians(name: string, genre: string, minAge: string, maxAge: string) {
        return this.http.get(`${this.baseUrl}/${this.musiciansUrl}/Filter`,
            {
                params:
                {
                    Name: name,
                    Genre: genre,
                    MinAge: minAge,
                    MaxAge: maxAge
                }
            });
    }
    public filterAlbums(name: string, minYear: string, maxYear: string, parentId: number) {
        return this.http.get(`${this.baseUrl}/${this.albumsUrl}/Filter/${parentId}`,
            {
                params:
                {
                    Name: name,
                    MinReleaseYear: minYear,
                    MaxReleaseYear: maxYear
                }
            })
    }
    public filterTracks(name: string, favoritesOnly: boolean, parentId: number) {
        return this.http.get(`${this.baseUrl}/${this.tracksUrl}/Filter/${parentId}`,
            {
                params:
                {
                    Name: name,
                    FavoritesOnly: favoritesOnly
                }
            })
    }
}