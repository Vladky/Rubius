﻿import { Component, ViewChild, ElementRef } from '@angular/core';
import { DataService } from "../../data.service";
import { MusicianComponent } from "../musician.component";
import { AlbumComponent } from "../../album/album.component";
import { AlbumListComponent } from "../../album/album-list/album-list.component";
import { DialogService } from "ng2-bootstrap-modal";

@Component({
    selector: 'musician-list',
    templateUrl: './musician-list.component.html',
    styleUrls: ['./musician-list.component.css']
})
/** musician-list component*/
export class MusicianListComponent {

    items: MusicianComponent[] = [];
    name: string = "";
    genre: string = "";
    minAge: string = "";
    maxAge: string = "";
    _showFilters: boolean;

    constructor(
        private service: DataService,
        private dialogService: DialogService
    ) { }

    ngOnInit() {
        this.service.getData().subscribe(result => {
            this.items = result.json() as MusicianComponent[];
        });
    }

    showAlbums(id: number) {
        let selectedItem = this.items.find(x => x.id == id);
        if (selectedItem != null) {
            let disposable = this.dialogService.addDialog(AlbumListComponent, {
                items: selectedItem.albums,
                title: `Albums of ${selectedItem.name}`,
                parent: selectedItem
            })
        }
    }

    filter() {
        this.service.filterMusicians(this.name, this.genre, this.minAge, this.maxAge).subscribe(result => {
            this.items = result.json() as MusicianComponent[]
        });
    }

    set showFilters(value: boolean) {
        this._showFilters = value;
    }
    get showFilters() {
        return this._showFilters;
    }
}