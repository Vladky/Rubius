﻿import { Component } from '@angular/core';
import { AlbumComponent } from "../album/album.component";

@Component({
    selector: 'musician',
    templateUrl: './musician.component.html',
    styleUrls: ['./musician.component.css']
})
/** musician component*/
export class MusicianComponent {
    /** musician ctor */
    constructor() {

    }
    public id: number;
    public name: string;
    public age: string;
    public genre: string;
    public careerStartYear: string;
    public albums: AlbumComponent[];
}