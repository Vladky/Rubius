﻿import { Component, Input } from '@angular/core';
import { TrackComponent } from "../track.component";
import { DialogService, DialogComponent } from "ng2-bootstrap-modal";
import { DataService } from "../../data.service";

export interface TrackListModel {
    items: TrackComponent[];
    title: string;
    albumId: number;
}
@Component({
    selector: 'track-list',
    templateUrl: './track-list.component.html',
    styleUrls: ['./track-list.component.css']
})
/** track-list component*/
export class TrackListComponent extends DialogComponent<TrackListModel, boolean> implements TrackListModel {
    @Input()
    items: TrackComponent[];
    title: string = "Tracks";
    albumId: number;

    name: string = "";
    favoritesOnly: boolean;
    _showFilters: boolean;

    constructor(
        dialogService: DialogService,
        private service: DataService
    ) {
        super(dialogService)
    }

    like(id: number) {
        let track: TrackComponent | any = this.items.find(x => x.id == id);
        this.service.like(id).subscribe(result => {
            this.items[this.items.indexOf(track)] = result.json() as TrackComponent
        })
    }

    addToFavorites(id: number) {
        let track: TrackComponent | any = this.items.find(x => x.id == id);
        this.service.addToFavorites(id).subscribe(result => {
            this.items[this.items.indexOf(track)] = result.json() as TrackComponent
        })
    }

    listen(id: number) {
        let track: TrackComponent | any = this.items.find(x => x.id == id);
        this.service.listen(id).subscribe(result => {
            this.items[this.items.indexOf(track)] = result.json() as TrackComponent;
        })
    }

    resetPlays() {
        this.service.resetPlays(this.albumId).subscribe(result => {
            //this.items = result.json().tracks as TrackComponent[];
            let resultTracks: TrackComponent[] = result.json().tracks as TrackComponent[];
            this.items.forEach(function (value) {
                value.isListened = (resultTracks.find(x => x.id === value.id) as TrackComponent).isListened;
            })
        })
    }

    filter() {
        this.service.filterTracks(this.name, this.favoritesOnly, this.albumId).subscribe(result => {
            this.items = result.json() as TrackComponent[]
        });
    }

    set showFilters(value: boolean) {
        this._showFilters = value;
    }
    get showFilters() {
        return this._showFilters;
    }
}