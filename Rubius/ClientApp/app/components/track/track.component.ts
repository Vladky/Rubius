﻿import { Component } from '@angular/core';

@Component({
    selector: 'track',
    templateUrl: './track.component.html',
    styleUrls: ['./track.component.css']
})
/** track component*/
export class TrackComponent {
    /** track ctor */
    constructor() {

    }
    public id: number;
    public name: string;
    public duration: string;
    public isFavorite: boolean;
    public isListened: boolean;
    public isLiked: boolean;
    public rating: number;
}