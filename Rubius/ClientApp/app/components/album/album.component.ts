﻿import { Component } from '@angular/core';
import { TrackComponent } from "../track/track.component";

@Component({
    selector: 'album',
    templateUrl: './album.component.html',
    styleUrls: ['./album.component.css']
})
/** album component*/
export class AlbumComponent {
    /** album ctor */
    constructor() {

    }
    public id: number;
    public name: string;
    public releaseYear: string;
    public tracks: TrackComponent[];
}