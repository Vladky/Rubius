﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AlbumListComponent } from './album-list.component';

let component: AlbumListComponent;
let fixture: ComponentFixture<AlbumListComponent>;

describe('album-list component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AlbumListComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AlbumListComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});