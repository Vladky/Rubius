﻿import { Component, Input } from '@angular/core';
import { AlbumComponent } from "../album.component";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { TrackComponent } from "../../track/track.component";
import { TrackListComponent } from "../../track/track-list/track-list.component";
import { MusicianComponent } from "../../musician/musician.component";
import { DataService } from "../../data.service";

export interface AlbumListModel {
    items: AlbumComponent[];
    title: string;
    parent: MusicianComponent;
}

@Component({
    selector: 'album-list',
    templateUrl: './album-list.component.html',
    styleUrls: ['./album-list.component.css']
})

export class AlbumListComponent extends DialogComponent<AlbumListModel, boolean> implements AlbumListModel {
    @Input()
    items: AlbumComponent[];
    title: string = "Albums";
    parent: MusicianComponent;

    name: string = "";
    minYear: string = "";
    maxYear: string = "";
    _showFilters: boolean;

    constructor(
        dialogService: DialogService,
        private service: DataService
    ) {
        super(dialogService)
    }

    showTracks(id: number) {
        let selectedItem = this.items.find(x => x.id == id);
        if (selectedItem != null) {
            let disposable = this.dialogService.addDialog(TrackListComponent, {
                items: selectedItem.tracks,
                title: `Tracks of ${selectedItem.name}`,
                albumId: selectedItem.id
            })
        }
    }

    filter() {
        this.service.filterAlbums(this.name, this.minYear, this.maxYear, this.parent.id).subscribe(result => {
            this.items = result.json() as AlbumComponent[]
        });
    }

    set showFilters(value: boolean) {
        this._showFilters = value;
    }
    get showFilters() {
        return this._showFilters;
    }
}