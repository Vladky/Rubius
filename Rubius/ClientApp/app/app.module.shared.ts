import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { MusicianListComponent } from "./components/musician/musician-list/musician-list.component";
import { MusicianComponent } from "./components/musician/musician.component";
import { TrackComponent } from "./components/track/track.component";
import { AlbumComponent } from "./components/album/album.component";
import { DataService } from "./components/data.service";
import { AlbumListComponent } from "./components/album/album-list/album-list.component";
import { TrackListComponent } from "./components/track/track-list/track-list.component";
import { BootstrapModalModule } from 'ng2-bootstrap-modal';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        MusicianListComponent,
        AlbumListComponent,
        TrackListComponent,
        MusicianComponent,
        AlbumComponent,
        TrackComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        BootstrapModalModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'musicians', pathMatch: 'full' },
            { path: 'musicians', component: MusicianListComponent },
            { path: '**', redirectTo: 'musicians' }
        ])
    ],
    entryComponents: [
        AlbumListComponent,
        TrackListComponent
    ],
    providers: [
        DataService
    ]
})
export class AppModuleShared {
}
